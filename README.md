# **Prácticas de Fundamentos de los Sistemas Inteligentes**  #

### Practica 1 ###

* Ramificación y acotación pertenece a las estrategias de búsqueda no informada
* Ramificación y acotación con subestimación pertenece a las estrategias de búsqueda informada


### Práctica 2: Redes Neuronales ###

* Flor IRIS
* Imágenes MNIST

### Practica 3: Aprendizaje por refuerzo ###

* Ejecutar y calcular la cantidad promedio de acciones que realiza el agente antes de llegar al objetivo.
* Modifica la política del agente para que alterne entre exploración y explotación.
  Implementa la opción greedy y ε-greedy (con distintos valores para el ε).
* Calcular y comparar el promedio de acciones que realiza el agente antes de llegar al
  objetivo con estas nuevas políticas.